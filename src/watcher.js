const fs = require("fs");
const path = require("path");
const chokidar = require("chokidar");

/*
Использовал chokidar потому что fs.watch у меня возвращал файл по 10 раз когда запускал скрипт
и по 2 раза каждый раз когда что-то изменялось
*/

const watcher = chokidar.watch(path.join(__dirname, "./dist"));

const userCsv = (type, data) => {
  if (data.type === "gas" || data.type === "water") {
    return fs.appendFile(
      path.join(__dirname, `./audit/${data.type}.csv`),
      `\n${data.date},${data.value},${data.name}`,
      () => console.log("File changed")
    );
  } else {
    throw new Error("File not write");
  }
};

const parsedJson = () => {
  const changedFile = path.join(__dirname, "./dist", "user.json");
  const rawData = fs.readFileSync(changedFile);
  const parseData = JSON.parse(rawData.toString());
  return parseData;
};

const deleteJson = () => {
  const delJson = fs.unlink(
    `${path.join(__dirname, "./dist", "user.json")}`,
    () => console.log("JSON deleted")
  );
};

watcher
  .on("add", (eventName) => {
    parsedJson();
    userCsv(parsedJson().type, parsedJson());
    deleteJson();
  })
  .on("change", (eventName) => {
    parsedJson();
    userCsv(parsedJson().type, parsedJson());
    deleteJson();
  });

const readLine = require("readline");
const process = require("process");
const path = require("path");
const fs = require("fs");

const defaultData = {
  type: "",
  value: 0,
  date: `${new Date().getMonth()}-${new Date().getDay()}-${new Date().getFullYear()}`,
  name: "",
};

const userDatas = JSON.parse(JSON.stringify(defaultData));

const rl = readLine.createInterface({
  output: process.stdout,
  input: process.stdin,
});

const userType = () => {
  return new Promise((resolve, reject) => {
    rl.question("Enter your type: ", (type) => {
      if (
        type.toLowerCase().trim() === "water" ||
        type.toLowerCase().trim() === "gas"
      ) {
        resolve(type.toLowerCase());
      } else {
        reject(new Error("Wrong type."));
      }
    });
  });
};

const userValue = () => {
  return new Promise((resolve, reject) => {
    rl.question("Enter your value: ", (value) => {
      if (value.match("^[0-9]{1,6}$")) {
        resolve(value);
      } else {
        reject(new Error("Wrong value."));
      }
    });
  });
};

const userName = () => {
  return new Promise((resolve, reject) => {
    rl.question("Enter your name: ", (name) => {
      if (name) {
        resolve(name);
      } else {
        reject(new Error("Wrong name."));
      }
    });
  });
};

const userQuestion = async () => {
  userDatas.type = await userType();
  userDatas.value = await userValue();
  userDatas.name = await userName();
};

const createJson = () => {
  fs.writeFile(
    path.join(__dirname, "dist", "user.json"),
    JSON.stringify(userDatas),
    { flag: "w+" },
    (err) => {
      if (err) console.log(err);
    }
  );
};

const askUser = async () => {
  await userQuestion();
  rl.close();
};

askUser().then(() => {
  createJson();
});
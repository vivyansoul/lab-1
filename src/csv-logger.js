const readLine = require("readline");
const fs = require("fs");
const path = require("path");

const userData = [];
let usersStats = {};

const arrPush = (data) => {
  userData.push(data.split(","));
};

const csvConvert = (data) => {
  return data
    .slice(data[0].length < 2 ? 1 : 0)
    .split("\n")
    .map((r) => r.split(","));
};

const rawData = (type) => {
  const rawFile = fs
    .readFileSync(path.join(__dirname, "./audit", `${type}.csv`))
    .toString();

  const parseFile = csvConvert(rawFile);

  for (let val of parseFile) {
    const arr = parseFile[parseFile.indexOf(val)];
    arr.push(type);
    userData.push(arr);
  }
};

const showData = (data) => {
  const tableArr = [];
  for (let i = 0; i < data.length; i++) {
    const userObj = {};
    userObj.date = data[i][0];
    userObj.value = data[i][1];
    userObj.name = data[i][2];
    userObj.type = data[i][3];
    tableArr.push(userObj);
  }

  tableArr.forEach((user) => {
    if (typeof usersStats[user.name] === "undefined") {
      usersStats[user.name] = [];
    }

    usersStats[user.name].push({
      type: user.type,
      date: user.date,
      value: user.value,
    });
  });

  Object.keys(usersStats).forEach((u) => {
    console.log(`\nUser: ${u} provided info ${usersStats[u].length} times`);
    usersStats[u].forEach((us) => {
      console.log(`${us["type"]}: ${us["value"]} date:${us["date"]}`);
    });
  });
};

rawData("gas");
rawData("water");
showData(userData);
